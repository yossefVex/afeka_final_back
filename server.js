const express = require("express");
const bosyParser = require("body-parser");
const cors = require("cors");
const db = require("./app/models");
const { init } = require("./app/helpers/http-response.helper");
const httpResponseHelper = require("./app/helpers/http-response.helper");

const app = express();

app.use((req, res, next) => {
    init(res);
    next();
})

var corsOptions = {
    origin: "http://localhost:8081"
}

app.use(bosyParser.json());
app.use(bosyParser.urlencoded({ extended: true }));

db.sequelize.sync();
// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

app.get("/", (req, res) => {
    httpResponseHelper.OK({my:"data"}, "GOT IT");
});
require("./app/routes/users.routes")(app);
require("./app/routes/clerks.routes")(app);
require("./app/routes/branches.routes")(app);
const PORT = process.env.PORT || 5000;

app.use((error, req, res, next) => {
    error;
    res.json(error);
})

app.listen(PORT, () => {
    console.log(`Server runs on port ${PORT}.`);
});


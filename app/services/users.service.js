const db = require("../models");

const createUser = async (user, transaction) => {
    try {
        const user = await db.users.create(user, {transaction});
        return user;
    } catch (error) {
        throw error;
    }
}

module.exports = {
    createUser
}
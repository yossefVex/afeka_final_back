const db = require("../models")

const createClerk = async (userid, branch, transaction) => {
    try {
        const clerk = db.clerks.create({ id: userid, branch }, { transaction });
        return clerk;
    } catch (error) {
        throw error;
    }
}

const findClerksByNameAC = async (fullName, condition) => {
    try {
        const clerks = await db.clerks.findAll({ where: condition });
        return clerks;
    } catch (error) {
        throw error;
    }
}

const findClerkByID = async (userID) => {
    try {
        const clerk = await db.clerks.findOne({ where: { userID } })
        return clerk;
    } catch (error) {
        throw error;
    }
}
const updateClerkByID = async (data, userID) => {
    try {
        const clerk = await db.clerks.update(data, { where: { userID } })
        return clerk;
    } catch (error) {
        throw error;
    }
}
const deleteClerkByID = async (userID) => {
    try {
        const clerk = await db.clerks.destroy({ where: { userID: userID } })
        return clerk;
    } catch (error) {
        throw error;
    }
}
const deleteAllClerks = async (userID) => {
    try {
        const clerk = await db.clerks.destroy({ where: { userID }, truncate: false })
        return clerk;
    } catch (error) {
        throw error;
    }
}

module.exports = {
    createClerk,
    findClerksByNameAC,
    findClerkByID,
    updateClerkByID,
    deleteClerkByID,




}
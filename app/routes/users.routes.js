module.exports = app =>{
    const users = require("../controllers/users.controller.js")
    const router = require("express").Router();

    router.post("/", users.create);
    router.get("/", users.findAll);
    router.get("/:userID", users.findOne);
    router.put("/:userID", users.update);
    router.delete("/:userID", users.delete);
    router.delete("/", users.deleteAll);
    app.use('/api/users', router);
}
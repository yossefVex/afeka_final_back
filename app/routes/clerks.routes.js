const db = require("../models/index.js");
const ClerksController = require("../controllers/clerks.controller");

// }
module.exports = app => {
    const router = require("express").Router();
    router.post("/", ClerksController.createClerk);
    router.get("/", ClerksController.findClerksByNameAC);
    router.put("/:userID", ClerksController.updateClerkByID);
    router.delete("/:userID", ClerksController.deleteClerkByID);
    router.delete("/", ClerksController.deleteAllClerks);
    app.use('/api/clerks', router);
}
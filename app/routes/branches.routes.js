module.exports = app =>{
    const branches = require("../controllers/branches.controller.js")
    const router = require("express").Router();

    router.post("/", branches.create);
    router.get("/", branches.findAll);
    router.get("/:branchID", branches.findOne);
    router.put("/:branchID", branches.update);
    router.delete("/:branchID", branches.delete);
    router.delete("/", branches.deleteAll);
    app.use('/api/branches', router);
}
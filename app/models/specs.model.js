module.exports = (sequelize, Sequelize) => {
    const Specs = sequelize.define("specs", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
              notNull: { msg: "spec name is required" },
            },
        }
    });
    return Specs;
};
module.exports = (sequelize, Sequelize) => {
    const Doctors = sequelize.define("doctors", {
        userID: {
            type: Sequelize.INTEGER,
            // refe
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
              notNull: { msg: "branch name is required" },
            },
        }
    });
    Doctors.belongsTo(sequelize.model("users"));
    Doctors.hasMany(sequelize.model("specs"), { onDelete: 'cascade', hooks: true });
    sequelize.model("specs").belongsTo(Doctors);
    return Doctors;
};
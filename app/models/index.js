const dbConfig = require("../config/db.config.js");

const { Sequelize } = require('sequelize');
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    define: {
        freezeTableName: true
    },
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./users.model.js")(sequelize, Sequelize);
db.branches = require("./branches.model.js")(sequelize, Sequelize);
db.clerks = require("./clerks.model.js")(sequelize, Sequelize);
db.clerks = require("./specs.model.js")(sequelize, Sequelize);
db.clerks = require("./doctors.model.js")(sequelize, Sequelize);

module.exports = db;
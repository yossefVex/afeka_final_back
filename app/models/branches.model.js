module.exports = (sequelize, Sequelize) => {
    const Branches = sequelize.define("branches", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
            validate: {
              notNull: { msg: "branch name is required" },
            },
        }
    });
    return Branches;
};
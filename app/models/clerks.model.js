/** the variables should be outside the module or inside ?? */


module.exports = (sequelize, Sequelize) => {
    const Clerks = sequelize.define("clerks", {
        branch: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.model('branches'),
                key: 'id',
            }
        },
        userID: {
            type: Sequelize.INTEGER,
            references: {
                model: sequelize.model('users'),
                key: 'userID'
            }
        }
    });
    // Clerks.belongsTo(sequelize.model("users"));
    // Clerks.belongsTo(sequelize.model("branches"));
    return Clerks;
};
let res;
/**
 * @param {number} statusCode
 * @param {any} data
 * @param {string} message
 */
const returnResponse = (statusCode, data, message) => {
    res.status(statusCode).json({data, message})
};

const init = response => {
    res = response;
};

const HttpStatusCodes = {
	OK: 200,
	CREATED: 201,
	ACCEPTED: 202,
	BADREQUEST: 400,
	NOTFOUND: 404,
	FORBIDDEN: 403,
	UNAUTHORIZED: 401,
	SERVERERROR: 500
}

module.exports = {
    init,
	HttpStatusCodes,
	OK: (data, msg = 'OK') => returnResponse(HttpStatusCodes.OK, data, msg),
	CREATED: (data, msg = 'Created') => returnResponse(HttpStatusCodes.CREATED, data, msg),
	ACCEPTED: (data, msg = 'Accepted') => returnResponse(HttpStatusCodes.ACCEPTED, data, msg),
	BADREQUEST: (data, msg = 'Bad Request') => returnResponse(HttpStatusCodes.BADREQUEST, data, msg),
	NOTFOUND: (data, msg = 'Not Found') => returnResponse(HttpStatusCodes.NOTFOUND, data, msg),
	FORBIDDEN: (data, msg = "Forbidden") => returnResponse(HttpStatusCodes.FORBIDDEN, data, msg),
	UNAUTHORIZED: (data, msg = 'Unauthorized') =>
		returnResponse(HttpStatusCodes.UNAUTHORIZED, data, msg),
	SERVERERROR: (data, msg = 'Internal Server Error') =>
		returnResponse(HttpStatusCodes.SERVERERROR, data, msg),
};
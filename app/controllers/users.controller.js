const db = require("../models");
const httpResponseHelper = require("../helpers/http-response.helper");
const User = db.users;
const Op = db.Sequelize.Op;

const userCreate = async (req, res) => {
    try {
        if (!req.body.fullName || !req.body.password) {
            return res.status(400).send({
                msg: "Content must include userID, fullName and password"
            });
        }
        const user = req.body
        const ans = await User.create(user)
        return httpResponseHelper.OK(ans, "OK created");
    } catch (error) {
        httpResponseHelper.SERVERERROR({error}, 'FAILED');
    }
}

exports.create = async (req, res) => {
    try {
        debugger;
        if (!req.body.fullName || !req.body.password) {
            return res.status(400).send({
                msg: "Content must include userID, fullName and password"
            });
        }
        const user = req.body
        const ans = await User.create(user)
        res.send(ans);
    } catch (err) { errHandler(err, res) }
};

exports.findAll = async (req, res) => {
    const fullName = req.query.fullName;
    const condition = fullName ? { fullName: { [Op.like]: `%{fullName}` } } : null;
    const ans = await User.findAll({ where: condition }).catch(err => errHandler(err, res))
    if (ans) res.send(ans);
};

exports.findOne = async (req, res) => {
    const userID = req.params.userID;
    const ans = await User.findByPk(userID).catch(err => errHandler(err, res))
    if (ans) res.send(ans);
};

exports.update = async (req, res) => {
    debugger;
    const userID = req.params.userID;
    const ans = await User.update(req.body, {
        where: { userID: userID }
    }).catch(err => { errHandler(err, res); return; });
    if (ans) returnMessage(res, ans, "updated");
};

exports.delete = async (req, res) => {
    const userID = req.params.userID;
    const ans = await User.destroy({ where: { userID: userID } })
        .catch(err => { errHandler(err, res); return; });
    if (ans) returnMessage(res, ans, "deleted");
};

exports.deleteAll = async (req, res) => {
    const ans = await User.destroy({
        where: {},
        truncate: false
    }).catch(err => { errHandler(err, res); return; });
    if (ans) returnMessage(res, ans, "", "All users were deleted !");

};
const returnMessage = function (res, ans, action = "action", difMessage) {
    if (!ans) return;
    if (difMessage) {
        return res.send({ message: difMessage });
    }
    if (ans == 1) {
        return res.send({ msg: `User(s) ${action} successfully !` });
    }
    res.send({ message: `Cannot ${action} User with id=${userID}. Maybe User was not found` });
}
const errHandler = function (err, res) {
    res.status(500).send({
        msg: err.message || 'error in User action !'
    })
}
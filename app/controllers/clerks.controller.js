const { createUser } = require("../services/users.service");
const clerksService = require("../services/clerks.service");
const httpResponseHelper = require("../helpers/http-response.helper");
const db = require("../models");
const Clerk = db.clerks;
const Op = db.Sequelize.Op;


// Create and Save a new Tutorial
exports.createClerk = async (req, res) => {
    const user = req.body;
    const branchID = req.body.branchID;
    const transaction = await db.sequelize.transaction();
    try {
        const userCreated = await createUser(user, transaction);
        const clerk = await clerksService.createClerk(userCreated.userID, branchID, transaction);
        await transaction.commit();
        httpResponseHelper.OK(clerk, "Clerk created !");
    } catch (error) {
        if (transaction)
            await transaction.rollback();
        throw error;
    }
};

exports.findClerksByNameAC = async (req, res) => {
    try {
        const fullName = req.params.fullName;
        const condition = fullName ? { fullName: { [Op.like]: `%{fullName}` } } : null;
        const clerks = await clerksService.findClerksByNameAC(fullName, condition)
        httpResponseHelper.OK(clerks);
    } catch (error) {
        throw error;
    }
};

exports.findClerkByID = async (req, res) => {
    const userID = req.params.userID;
    try {
        const clerk = await clerksService.findClerkByID(userID)
        httpResponseHelper.OK(clerk);
    } catch (error) {
        throw error;
    }
};

exports.updateClerkByID = async (req, res) => {
    const userID = req.params.userID;
    try {
        const clerk = await clerksService.updateClerkByID(req.body, userID)
        httpResponseHelper.OK(clerk);
    } catch (error) {
        throw error;
    }
};

exports.deleteClerkByID = async (req, res) => {
    const userID = req.params.userID;
    try {
        const clerk = await clerksService.deleteClerkByID(userID)
        httpResponseHelper.OK(clerk, "Clerk deleted successfully");
    } catch (error) {
        throw error;
    }
};
exports.deleteAllClerks = async (req, res) => {
    const userID = req.params.userID;
    try {
        const clerk = await clerksService.deleteAllClerks()
        httpResponseHelper.OK(clerk, "All Clerks deleted successfully");
    } catch (error) {
        throw error;
    }
};

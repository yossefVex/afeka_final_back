const db = require("../models");
const Branches = db.branches;
const Op = db.Sequelize.Op;


exports.create = async (req, res) => {
    if (!req.body.branchName) {
        return res.status(400).send({
            msg: "Content must include branch name"
        });
    }
    const branch = {
        name: req.body.branchName
    }
    const ans = await Branches.create(branch).catch(err => errHandler(err, res))
    if (ans) res.send(ans);
};

exports.findAll = async (req, res) => {
    const name = req.query.branchName;
    const condition = name ? { name: { [Op.like]: `%{name}` } } : null;
    const ans = await Branches.findAll({ where: condition }).catch(err => errHandler(err, res))
    if (ans) res.send(ans);
};

exports.findOne = async (req, res) => {
    const branchID = req.params.branchID;
    const ans = await Branches.findByPk(branchID).catch(err => errHandler(err, res))
    if (ans) res.send(ans);
};

exports.update = async (req, res) => {
    const branchID = req.params.branchID;
    const ans = await Branches.update({
        name: req.body.branchName
    }, { where: { id: branchID } })
        .catch(err => { errHandler(err, res); return; });
    if (ans) returnMessage(res, ans, "updated");
};

exports.delete = async (req, res) => {
    const branchID = req.params.branchID;
    const ans = await Branches.destroy({ where: { id: branchID } })
        .catch(err => { errHandler(err, res); return; });
    if (ans) returnMessage(res, ans, "deleted");
};

exports.deleteAll = async (req, res) => {
    const ans = await Branches.destroy({
        where: {},
        truncate: false
    }).catch(err => { errHandler(err, res); return; });
    if (ans) returnMessage(res, ans, "", "All branches were deleted !");

};
const returnMessage = function (res, ans, action = "action", difMessage) {
    if (!ans) return;
    if (difMessage) {
        return res.send({ message: difMessage });
    }
    if (ans == 1) {
        return res.send({ msg: `Branch(es) ${action} successfully !` });
    }
    res.send({ message: `Cannot ${action} Branch with id=${userID}. Maybe User was not found` });
}
const errHandler = function (err, res) {
    res.status(500).send({
        msg: err.message || 'error in Branch action !'
    })
}